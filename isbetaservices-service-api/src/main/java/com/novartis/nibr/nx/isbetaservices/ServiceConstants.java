package com.novartis.nibr.nx.isbetaservices;


public interface ServiceConstants {

	String SCHEMA = "ISBETADB";
	String PERSISTENCE_UNIT_NAME = "ISBETASERVICES";
	String FQPN = "com.novartis.nibr.nx.isbetaservices";
	
	String COMPONENT_PREFIX = "isbetaservices-";
	String ENTITY_MANAGER_FACTORY_NAME = COMPONENT_PREFIX + "entityManagerFactory";
	String TRANSACTION_MANAGER_NAME = COMPONENT_PREFIX + "transactionManager";

}
