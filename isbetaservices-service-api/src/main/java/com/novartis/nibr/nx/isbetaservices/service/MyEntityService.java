package com.novartis.nibr.nx.isbetaservices.service;


import java.util.List;

import org.springframework.data.domain.Pageable;

import com.novartis.nibr.nx.isbetaservices.entity.MyEntity;


public interface MyEntityService {
	
	List<MyEntity> findAll ();
	List<MyEntity> findAll (Pageable pageable);

	MyEntity findOne (Long entityId);
	MyEntity findByEntityName (String entityName);
	
	MyEntity save (MyEntity entity);
	
	void delete (Long entityId);
}
