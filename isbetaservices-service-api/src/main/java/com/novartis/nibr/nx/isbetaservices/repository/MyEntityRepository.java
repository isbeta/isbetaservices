package com.novartis.nibr.nx.isbetaservices.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.novartis.nibr.nx.isbetaservices.entity.MyEntity;


public interface MyEntityRepository 
	extends JpaRepository<MyEntity,Long> {

	MyEntity findByEntityName (String entityName);
}
