package com.novartis.nibr.nx.isbetaservices.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.novartis.nibr.nx.services.entity.AbstractAuditedEntity;


//  basic JPA and JAXB annotated object
@XmlRootElement
@JsonInclude(Include.NON_NULL)
@Entity
@Table(name=MyEntity.TABLE_ENTITY)
@SequenceGenerator(name=MyEntity.GENERATOR_ENTITY_ID, sequenceName=MyEntity.COLUMN_ENTITY_ID + "_SEQ")
public class MyEntity 
	extends AbstractAuditedEntity<MyEntity,Long> {
	
    private static final long serialVersionUID = 1L;
    
    public static final String TABLE_ENTITY = "MY_ENTITY";
	public static final String COLUMN_ENTITY_ID = "ENTITY_ID";

    public static final String GENERATOR_ENTITY_ID = "EntityIdGenerator";
    
	@Id
	@GeneratedValue(generator=GENERATOR_ENTITY_ID)
	@Column(name=MyEntity.COLUMN_ENTITY_ID)
	private Long entityId;
	
	@Column(name="ENTITY_NAME")
	private String entityName;
	
	@Override
	public Long getEntityId () {
		return entityId;
	}
	
	public void setEntityId (final Long entityId) {
		this.entityId = entityId;
	}

	public String getEntityName () {
		return entityName;
	}
	
	public void setEntityName (final String entityName) {
		this.entityName = entityName;
	}
	
}
