package com.novartis.nibr.nx.isbetaservices.test;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.novartis.nibr.nx.isbetaservices.ServiceConstants;
import com.novartis.nibr.nx.isbetaservices.config.EntityManagerDataSource;

import com.novartis.nibr.nx.services.test.AbstractTestDataLoader;


/**
 * a component to execute the Liquibase change sets and load the test data.  
 * 
 * we may want to restrict this component to the ds-embedded profile at some point soon.
 * 
 * @author ringgjo1
 */
@Component
@Profile("ds-embedded")
public class TestDataLoader 
	extends AbstractTestDataLoader  {

	
	/**
	 * request injection of the application data source
	 */
	@Autowired
	public TestDataLoader(
			final EntityManagerDataSource dataSource) {
		super(dataSource, 
				ServiceConstants.SCHEMA, 
				"com/novartis/nibr/nx/isbetaservices/liquibase-changelog.xml");
	}
}
