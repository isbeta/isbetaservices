package com.novartis.nibr.nx.isbetaservices.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.novartis.nibr.nx.isbetaservices.ServiceConstants;
import com.novartis.nibr.nx.isbetaservices.config.BaseTests;
import com.novartis.nibr.nx.isbetaservices.entity.MyEntity;

import org.testng.annotations.Test;
import static org.testng.Assert.*;


@Test
public class MyEntityServiceImplTests 
	extends BaseTests {

	@Autowired MyEntityService myEntityService;
		
	@Test
	public void testFindAll () {
		
		final List<MyEntity> list = myEntityService.findAll();
		
		assertFalse(list.isEmpty());
	}

	@Test
	public void testFindAllPaged () {
		
		final PageRequest pr = new PageRequest(0, 1, new Sort("entityId"));
		final List<MyEntity> list = myEntityService.findAll(pr);
		
		assertEquals(list.size(), 1);
		assertEquals(list.get(0).getEntityId(), Long.valueOf(1001));
	}

	@Test
	public void testFindOne () {
		
		final MyEntity me = myEntityService.findOne(1001L);
		
		assertEquals(me.getEntityName(), "my_entity1001");
	}

	@Test
	public void testFindByEntityName () {
		
		final MyEntity me = myEntityService.findByEntityName("my_entity1001");
		
		assertEquals(me.getEntityId(), Long.valueOf(1001));
	}
	
	@Test
	public void testSave() {
		
		final MyEntity me = new MyEntity();
		me.setEntityName("my_entity1006");
		
		myEntityService.save(me);
		
		final MyEntity x = myEntityService.findOne(1006L);
		assertEquals(x.getEntityName(), "my_entity1006");
	}

	@Test
	public void testDelete() {
		
		myEntityService.delete(1004L);
		
		final MyEntity x = myEntityService.findOne(1004L);
		assertNull(x);
	}

}
