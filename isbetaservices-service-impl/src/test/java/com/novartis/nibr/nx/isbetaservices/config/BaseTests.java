package com.novartis.nibr.nx.isbetaservices.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.AuditorAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;


@ContextConfiguration
public class BaseTests 
	extends AbstractTestNGSpringContextTests {

	@Configuration
	@ComponentScan(
		basePackages={
			"com.novartis.nibr.nx.isbetaservices",
			"com.novartis.nibr.nx.services.timing"
			}, 
		nameGenerator=BeanNameGenerator.class)
	@EnableSpringConfigured
	@ImportResource({"classpath:/com/novartis/nibr/nx/isbetaservices/repository/application-context-tx.xml"})
	static class Config {
		
		/**
		 * replacement for xml <context:property-placeholder> 
		 */
		@Bean
		public PropertySourcesPlaceholderConfigurer testProperties () {
			
			final PropertySourcesPlaceholderConfigurer pc = new PropertySourcesPlaceholderConfigurer();
			pc.setLocation(new ClassPathResource("/tests.properties"));
			return pc;
		}

		@Bean
		public AuditorAware<String> auditorAware () {
			
			return new AuditorAware<String>() {

				@Override
				public String getCurrentAuditor () {
					return "TESTUSR1";
				}
			};
		}
	}
}
