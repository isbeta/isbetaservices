package com.novartis.nibr.nx.isbetaservices.repository;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.novartis.nibr.nx.isbetaservices.ServiceConstants;
import com.novartis.nibr.nx.isbetaservices.config.BaseTests;
import com.novartis.nibr.nx.isbetaservices.entity.MyEntity;

import org.testng.annotations.Test;
import static org.testng.Assert.*;


@Test
@TestExecutionListeners({TransactionalTestExecutionListener.class})
@Transactional(ServiceConstants.TRANSACTION_MANAGER_NAME)
public class MyEntityRepositoryImplTests 
	extends BaseTests {

	@Autowired MyEntityRepository myEntityRepository;

	@Test
	public void testFindAll () {
		
		final List<MyEntity> list = myEntityRepository.findAll();
		
		assertFalse(list.isEmpty());
	}

	@Test
	public void testFindAllPaged () {
		
		final PageRequest pr = new PageRequest(0, 1, new Sort("entityId"));
		final Page<MyEntity> page = myEntityRepository.findAll(pr);
		
		assertEquals(page.getSize(), 1);
		assertEquals(page.getNumber(), 0);
		assertEquals(page.getContent().get(0).getEntityId(), Long.valueOf(1001));
	}

	@Test
	public void testFindOne () {
		
		final MyEntity me = myEntityRepository.findOne(1001L);
		
		assertEquals(me.getEntityName(), "my_entity1001");
	}

	@Test
	public void testFindByEntityName () {
		
		final MyEntity me = myEntityRepository.findByEntityName("my_entity1001");
		
		assertEquals(me.getEntityId(), Long.valueOf(1001));
	}
	
	@Test
	public void testSave() {
		
		final MyEntity me = new MyEntity();
		me.setEntityName("my_entity1005");
		
		myEntityRepository.save(me);
		myEntityRepository.flush();
		
		final MyEntity x = myEntityRepository.findOne(1005L);
		assertEquals(x.getEntityName(), "my_entity1005");
	}

	@Test
	public void testDelete() {
		
		myEntityRepository.delete(1003L);
		myEntityRepository.flush();
		
		final MyEntity x = myEntityRepository.findOne(1003L);
		assertNull(x);
	}

}
