package com.novartis.nibr.nx.isbetaservices.service.impl;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.novartis.nibr.nx.isbetaservices.ServiceConstants;


/**
 * convenience annotation for when only a read-only, participating tranactional service is needed
 *  
 * @author ringgjo1
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Service
@Transactional(value=ServiceConstants.TRANSACTION_MANAGER_NAME, readOnly=true, propagation=Propagation.SUPPORTS)
public @interface TransactionalReadOnlyService {

}
