package com.novartis.nibr.nx.isbetaservices.config;


import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import com.novartis.nibr.nx.services.config.DelegatingEntityManagerDataSource;
import com.novartis.nibr.nx.isbetaservices.ServiceConstants;


@Component
@Profile("ds-embedded")
public class HSQLDBEntityManagerDataSource 
	extends DelegatingEntityManagerDataSource
	implements EntityManagerDataSource {
	
	private static final Logger LOG = LoggerFactory.getLogger(HSQLDBEntityManagerDataSource.class);
	
	
	@Autowired
	public HSQLDBEntityManagerDataSource (
			@Value("${isbetaservices.ds.schema:" + ServiceConstants.SCHEMA + "}") final String schemaName) {
		super(dataSource(schemaName));
	}

	private static DataSource dataSource (final String schemaName) {
		
		final DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName("org.hsqldb.jdbcDriver");
		ds.setUrl("jdbc:hsqldb:mem:isbetaservices");
		ds.setUsername("sa");
		
		LOG.info("creating schema: '{}' in embedded HSQLDB instance", schemaName);
		
		try {
			
			new JdbcTemplate(ds).execute("CREATE SCHEMA " + schemaName + " AUTHORIZATION DBA");
		} catch (final DataAccessException e) {
			
			final String msg = "unable to create schema in HSQLDB instance";
			LOG.error(msg, e);
			throw new IllegalStateException(msg, e);
		}

		return ds;
	}

	@Override
	public Map<String, Object> customizeJpaPropertyMap (final Map<String, Object> map) {
		
		map.put("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
		map.put("hibernate.hbm2ddl.auto", "create");
		map.put("hibernate.connection.pool_size", "1");		
		map.put("hibernate.generate_statistics", "true");

		return map;
	}

}
