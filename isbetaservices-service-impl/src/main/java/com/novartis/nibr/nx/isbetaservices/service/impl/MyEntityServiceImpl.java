package com.novartis.nibr.nx.isbetaservices.service.impl;


import java.util.List;

import org.perf4j.aop.Profiled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.novartis.nibr.nx.isbetaservices.ServiceConstants;
import com.novartis.nibr.nx.isbetaservices.entity.MyEntity;
import com.novartis.nibr.nx.isbetaservices.repository.MyEntityRepository;
import com.novartis.nibr.nx.isbetaservices.service.MyEntityService;


@Service
@Transactional(value=ServiceConstants.TRANSACTION_MANAGER_NAME)
public class MyEntityServiceImpl 
	implements MyEntityService {
	
	private final MyEntityRepository myEntityRepository;
	
	@Autowired
	public MyEntityServiceImpl (final MyEntityRepository myEntityRepository) {
		super();
		
		this.myEntityRepository = myEntityRepository;
	}
	
	@Profiled
	@Override
	public List<MyEntity> findAll () {
		return findAll(null);
	}

	@Profiled
	@Override
	public List<MyEntity> findAll (final Pageable pageable) {
		return myEntityRepository.findAll(pageable).getContent();
	}

	@Profiled
	@Override
	public MyEntity findOne (final Long entityId) {
		return myEntityRepository.findOne(entityId);
	}
	
	@Profiled
	@Override
	public MyEntity findByEntityName (final String entityName) {
		return myEntityRepository.findByEntityName(entityName);
	}
	
	@Transactional(ServiceConstants.TRANSACTION_MANAGER_NAME)
	@Profiled
	@Override
	public MyEntity save (final MyEntity entity) {
		
		final MyEntity me = myEntityRepository.save(entity);
		myEntityRepository.flush();
		return me;
	}
	
	@Transactional(ServiceConstants.TRANSACTION_MANAGER_NAME)
	@Profiled
	@Override
	public void delete (final Long entityId) {
		myEntityRepository.delete(entityId);
		myEntityRepository.flush();
	}

}
