package com.novartis.nibr.nx.isbetaservices.config;


import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import com.google.common.collect.Maps;

import com.novartis.nibr.nx.isbetaservices.ServiceConstants;


@Configuration
@EnableJpaRepositories(
		basePackages=ServiceConstants.FQPN,
		entityManagerFactoryRef=ServiceConstants.ENTITY_MANAGER_FACTORY_NAME,
		transactionManagerRef=ServiceConstants.TRANSACTION_MANAGER_NAME)
@EnableJpaAuditing
public class EntityManagerConfiguration {
	
	/**
	 * used to request injection of the 'isbetaservices.ds.schema' property, to allow overriding from configuration 
	 * properties files or the command line.  if the property value is not specified, the expression will evaluate 
	 * to ServiceConstants.SCHEMA.
	 */
	private static final String DS_SCHEMA_VALUE_EXPRESSION = "${isbetaservices.ds.schema:" + ServiceConstants.SCHEMA + "}";
		
	
	@Bean(name=ServiceConstants.ENTITY_MANAGER_FACTORY_NAME)
	@DependsOn("org.springframework.context.config.internalBeanConfigurerAspect")  //  secret sauce for making sure anything @Configurable runs first
	public LocalContainerEntityManagerFactoryBean entityManagerFactory (
			final EntityManagerDataSource dataSource,
			@Value(DS_SCHEMA_VALUE_EXPRESSION) final String schemaName) {
		
		//  instantiate factory bean which will create an instance of the provider's 
		//  EntityManagerFactory implementation class.  that factory will in turn create 
		//  thread-bound instances of EntityManager to perform all persistence operations.
		final LocalContainerEntityManagerFactoryBean fb = new LocalContainerEntityManagerFactoryBean();

		fb.setPackagesToScan(ServiceConstants.FQPN);
		fb.setPersistenceUnitName(ServiceConstants.PERSISTENCE_UNIT_NAME);
		fb.setDataSource(dataSource);
		fb.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		
		final Map<String,String> jp = Maps.newHashMap();
		
		//  Hibernate-specific property for setting the default schema name for JPA entity classes where it is not 
		//  specified.  this pattern is the best way to retain flexibility, for example if testing against a differently
		//  named schema is required.
		jp.put("hibernate.default_schema", schemaName);

		
		//  replace the default EhCacheRegionFactory of Hibernate with a custom implementation 
		//  managed by Spring, by way of @Configurable annotation and AspectJ weaving
		jp.put("hibernate.cache.region.factory_class", "com.novartis.nibr.nx.services.dao.SpringEhcacheRegionFactory");

		//  second-level caching can be configured with properties like this, or else with @Cacheable.  collections 
		//  still require setting by property since the JPA spec doesn't cover collection caching.
//		jp.put("hibernate.ejb.classcache.com.novartis.nibr.nx.isbetaservices.entity.MyEntity", "read-write");

		dataSource.customizeJpaPropertyMap(fb.getJpaPropertyMap());

		fb.setJpaPropertyMap(jp);
		
		return fb;
	}
	
	@Bean(name=ServiceConstants.TRANSACTION_MANAGER_NAME)
	public PlatformTransactionManager transactionManager (
			final EntityManagerDataSource dataSource,
			@Value(DS_SCHEMA_VALUE_EXPRESSION) final String schemaName) {
		
    	//  call instrumented config class method
    	final LocalContainerEntityManagerFactoryBean fb = entityManagerFactory(dataSource, schemaName);
		return new JpaTransactionManager(fb.getObject());
	}

}
