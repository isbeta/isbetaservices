package com.novartis.nibr.nx.isbetaservices.config;


import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;

import com.novartis.nibr.nx.isbetaservices.ServiceConstants;


public class BeanNameGenerator 
	extends AnnotationBeanNameGenerator {

	@Override
	public String generateBeanName (
			final BeanDefinition definition,
			final BeanDefinitionRegistry registry) {
		return ServiceConstants.COMPONENT_PREFIX + super.generateBeanName(definition, registry);
	}

	
}
