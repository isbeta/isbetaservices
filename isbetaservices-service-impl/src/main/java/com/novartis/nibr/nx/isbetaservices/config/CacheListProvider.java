package com.novartis.nibr.nx.isbetaservices.config;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Component;

import net.sf.ehcache.Cache;

import com.google.common.collect.ImmutableList;

import com.novartis.nibr.nx.services.dao.AbstractCacheListProvider;
import com.novartis.nibr.nx.isbetaservices.entity.MyEntity;


@Component
public class CacheListProvider
	extends AbstractCacheListProvider {
	
	private static final Logger LOG = LoggerFactory.getLogger(CacheListProvider.class);
	
	@Override
	public List<Cache> getCacheList () {
		
		LOG.info("cache setup for isbetaservices module");
		
		//  configure entity caches

		return ImmutableList.<Cache>builder()
			.add(new Cache(defaultEntityCacheConfiguration().name(MyEntity.class.getName())))
			.build();
	}
}
