package com.novartis.nibr.nx.isbetaservices.config;


import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.novartis.nibr.nx.services.config.DelegatingEntityManagerDataSource;


@Component
@Profile({"ds-oracle","oracle-jdbc","oracle-jdbc-isbetaservices","oracle-ucp","oracle-ucp-isbetaservices"})
@ImportResource("classpath:com/novartis/nibr/nx/isbetaservices/repository/application-context-ds-oracle.xml")
public class OracleEntityManagerDataSource 
	extends DelegatingEntityManagerDataSource
	implements EntityManagerDataSource  {

	@Autowired
	public OracleEntityManagerDataSource (
			@Qualifier("isbetaservices-oracleDataSource") final DataSource dataSource) {
		super(dataSource);
	}

	@Override
	public Map<String, Object> customizeJpaPropertyMap (final Map<String, Object> map) {
		
		map.put("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");

		return map;
	}

}
