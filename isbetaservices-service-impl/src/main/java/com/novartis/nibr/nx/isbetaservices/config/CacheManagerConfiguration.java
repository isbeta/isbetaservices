package com.novartis.nibr.nx.isbetaservices.config;


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

import com.novartis.nibr.nx.services.dao.AbstractCacheManagerConfiguration;
import com.novartis.nibr.nx.services.dao.CacheListProvider;


@Configuration
@Profile({"ehcache-isbetaservices-default"})
public class CacheManagerConfiguration
	extends AbstractCacheManagerConfiguration {

	private static final Logger LOG = LoggerFactory.getLogger(CacheManagerConfiguration.class);
	
    //  get the cache folder location from the 'services.ehcache.folder' property, or else use a 
    //  default value under 'java.io.tmpdir' if not specified
	private static final String EHCACHE_FOLDER_EXPRESSION = 
			"${services.ehcache.folder:#{systemProperties['java.io.tmpdir']}/isbetaservices/ehcache}";
			
	@Bean(name="isbetaservices-cacheManager")
	public CacheManager cacheManager (
			final ApplicationContext context,
			@Value(EHCACHE_FOLDER_EXPRESSION) final String ehcacheFolder) {
		
		LOG.info("creating CacheManager with disk store location: {}", ehcacheFolder);
		
		final CacheManager cm = newCacheManager(ehcacheFolder);
		cm.setName("isbetaservices-default-cache-manager");
		
		final Map<String,CacheListProvider> map = context.getBeansOfType(CacheListProvider.class);
		for (final CacheListProvider clp : map.values()) {
			for (final Cache c : clp.getCacheList()) {
				cm.addCache(c);
				LOG.debug("added cache: {}", c.getName());
			}
		}
		
		return cm;
	}
}
