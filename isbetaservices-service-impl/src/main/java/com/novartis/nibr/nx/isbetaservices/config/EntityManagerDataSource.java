package com.novartis.nibr.nx.isbetaservices.config;


/**
 * provides a DataSource subclass that enables an implementing data source 
 * instance in a multi-DataSource application context to be autowired by 
 * type, rather than with a named qualifier.
 * 
 * @author ringgjo1
 */
public interface EntityManagerDataSource 
	extends com.novartis.nibr.nx.services.config.EntityManagerDataSource {

}
