package com.novartis.nibr.nx.isbetaservices.config;


import javax.ws.rs.ext.Provider;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.jaxrs.xml.JacksonJaxbXMLProvider;
import com.google.common.collect.Lists;

import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

import com.novartis.nibr.nx.services.rest.HostResource;
import com.novartis.nibr.nx.services.rest.VersionResource;

import net.novartis.nibr.healthcheck.Spring3HealthCheck;
import com.novartis.nibr.nx.isbetaservices.rest.v1.MyEntityResource;


@Configuration
public class TestServerConfig {
	
	public static final String ENDPOINT_ADDRESS = "local://test";

	
	@Autowired 
	private ApplicationContext context;

	@Bean(destroyMethod="destroy")
	public Server testServer () {
				
		final JAXRSServerFactoryBean sf = new JAXRSServerFactoryBean();
		sf.setServiceBeans(Lists.<Object>newArrayList(
				context.getBean(MyEntityResource.class),
				context.getBean(VersionResource.class),
				context.getBean(HostResource.class),
//				context.getBean(Spring3HealthCheck.class),
				context.getBean(ApiListingResource.class)));
		sf.setProviders(Lists.newArrayList(context.getBeansWithAnnotation(Provider.class).values()));
		sf.setAddress(ENDPOINT_ADDRESS);
		
		return sf.create();
	}
	
	@Bean
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public WebClient testClient () {
		return WebClient.create(ENDPOINT_ADDRESS, Lists.newArrayList(context.getBeansWithAnnotation(Provider.class).values()));
	}
	
	@Bean
	public JacksonJaxbJsonProvider jsonProvider () {
		return new JacksonJaxbJsonProvider();
	}

	@Bean
	public JacksonJaxbXMLProvider xmlProvider () {
		return new JacksonJaxbXMLProvider();
	}
	
	@Bean
	public ApiListingResource apiListingResource () {
		return new ApiListingResource();
	}

	@Bean
	public SwaggerSerializers swaggerSerializers () {
		return new SwaggerSerializers();
	}
}
