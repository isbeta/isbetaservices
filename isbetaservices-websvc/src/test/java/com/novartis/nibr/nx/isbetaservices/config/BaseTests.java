package com.novartis.nibr.nx.isbetaservices.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

import com.novartis.nibr.nx.isbetaservices.ServiceConstants;


@ContextConfiguration
public class BaseTests 
	extends AbstractTestNGSpringContextTests {

	
	@Configuration
	@ComponentScan(
		basePackages={
				ServiceConstants.FQPN,
				"com.novartis.nibr.nx.services.rest"}, 
		nameGenerator=BeanNameGenerator.class,
		excludeFilters={@ComponentScan.Filter(type=FilterType.ASSIGNABLE_TYPE, value=SwaggerConfig.class)})
	@EnableSpringConfigured
	@ImportResource({"classpath:com/novartis/nibr/nx/isbetaservices/repository/application-context-tx.xml"})
	static class Config {
		
		/**
		 * partial replacement for xml <context:annotation-config>, enables JSR-250 annotation support 
		 */
		@Bean
		public CommonAnnotationBeanPostProcessor commonAnnotationBeanPostProcessor () {
			return new CommonAnnotationBeanPostProcessor();
		}

		/**
		 * replacement for xml <context:property-placeholder> 
		 */
		@Bean
		public PropertySourcesPlaceholderConfigurer testProperties () {
			
			final PropertySourcesPlaceholderConfigurer pc = new PropertySourcesPlaceholderConfigurer();
			pc.setLocations(
					new ClassPathResource("/version.properties"),
					new ClassPathResource("/tests.properties"));
			return pc;
		}
	}
}
