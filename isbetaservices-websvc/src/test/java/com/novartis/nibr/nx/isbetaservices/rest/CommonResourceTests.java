package com.novartis.nibr.nx.isbetaservices.rest;


import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.novartis.nibr.nx.isbetaservices.config.BaseTests;

import org.testng.annotations.Test;

import static org.testng.Assert.*;


@Test
public class CommonResourceTests
	extends BaseTests {
	
	@Autowired ApplicationContext context;

	@Test
	public void testWADL () {
		
		final WebClient client = context.getBean(WebClient.class)
				.path("")
				.query("_wadl", "");
		
		assertEquals(client.get().getStatus(), Status.OK.getStatusCode());
		
		client.close();
	}

	@Test
	public void testVersion () {
		
		final WebClient client = context.getBean(WebClient.class)
				.accept("application/json")
				.path("version");
		
		assertEquals(client.get().getStatus(), Status.OK.getStatusCode());
		
		client.close();
	}

	@Test
	public void testHost () {
		
		final WebClient client = context.getBean(WebClient.class)
				.accept("application/json")
				.path("host");
		
		assertEquals(client.get().getStatus(), Status.OK.getStatusCode());
		
		client.close();
	}

//	@Test
//	public void testHealthCheck () {
//		
//		final WebClient client = context.getBean(WebClient.class)
//				.path("health/v1/capabilities");
//		
//		assertEquals(client.get().getStatus(), Status.OK.getStatusCode());
//		
//		client.close();
//	}
//
//	@Test
//	public void testSwagger () {
//		
//		final WebClient client = context.getBean(WebClient.class)
//				.path("api-docs");
//		
//		assertEquals(client.get().getStatus(), Status.OK.getStatusCode());
//		
//		client.close();
//	}
}
