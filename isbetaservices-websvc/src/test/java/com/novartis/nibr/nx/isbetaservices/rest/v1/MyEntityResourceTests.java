package com.novartis.nibr.nx.isbetaservices.rest.v1;


import static org.testng.Assert.*;

import java.util.Collection;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

import com.novartis.nibr.nx.isbetaservices.config.BaseTests;
import com.novartis.nibr.nx.isbetaservices.entity.MyEntity;

import org.testng.annotations.Test;
import static org.testng.Assert.*;


@Test
public class MyEntityResourceTests
	extends BaseTests {
	

	@Autowired ApplicationContext context;

	@Test
	public void testListMyEntities () {
		
		final WebClient client = context.getBean(WebClient.class)
				.accept("application/json")
				.path("v1/myEntities");
		
		final Collection<? extends MyEntity> c = client.getCollection(MyEntity.class);

		assertFalse(c.isEmpty());

		client.close();
	}

	@Test
	public void testListMyEntitiesPaged () {
		
		final WebClient client = context.getBean(WebClient.class)
				.accept("application/json")
				.path("v1/myEntities")
				.query("page", 1)
				.query("itemsPerPage", 1)
				.query("sort", "entityId:asc");
		
		final Collection<? extends MyEntity> c = client.getCollection(MyEntity.class);

		assertEquals(c.size(), 1);
		assertEquals(c.iterator().next().getEntityId(), Long.valueOf(1001));  //  can't actually guarantee this without sort...

		client.close();
	}

	@Test
	public void testFindOne () {
		
		final WebClient client = context.getBean(WebClient.class)
				.accept("application/json")
				.path("v1/myEntities/1001");
		
		final MyEntity e = client.get(MyEntity.class);

		assertEquals(e.getEntityId(), Long.valueOf(1001));

		client.close();
	}
	
	@Test
	public void testCreateOne () {
		  
		final WebClient client = context.getBean(WebClient.class)
				.header("NIBR521", "DUMMY")
				.type("application/json")
				.accept("application/json")
				.path("v1/myEntities");
		
		final Response r = client.post("{\"entityName\":\"awesome new entity\"}");
		assertEquals(r.getStatus(), Status.OK.getStatusCode());
		
		final MyEntity me = r.readEntity(MyEntity.class);
		
		assertEquals(me.getEntityName(), "awesome new entity");

		client.close();
	}

}
