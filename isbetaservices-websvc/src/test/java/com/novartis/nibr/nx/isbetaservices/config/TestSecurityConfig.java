package com.novartis.nibr.nx.isbetaservices.config;


import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


@Configuration
public class TestSecurityConfig {
	
	private static final Logger LOG = LoggerFactory.getLogger(BaseTests.class);

	
	@Value("${isbetaservices.test.user521}") private String default521;
	
	@PostConstruct
	public void init () {
		
		LOG.debug("setting system property 'spring.security.strategy' to {} during tests", SecurityContextHolder.MODE_GLOBAL);
		System.setProperty("spring.security.strategy", SecurityContextHolder.MODE_GLOBAL);
		
		final Authentication auth = new UsernamePasswordAuthenticationToken(default521, "");
		LOG.debug("setting default security auth to {} during tests", auth.getName());

		//  mock authentication during tests, for use by the configured AuditorAware instance.  this is used 
		//  in conjunction with setting the sys prop spring.security.strategy = MODE_GLOBAL for the surefire plugin.
		SecurityContextHolder.getContext().setAuthentication(auth);
	}
}
