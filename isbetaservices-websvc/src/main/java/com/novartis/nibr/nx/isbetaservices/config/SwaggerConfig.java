package com.novartis.nibr.nx.isbetaservices.config;

import io.swagger.jaxrs.config.BeanConfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.WebApplicationContext;

import com.novartis.nibr.nx.isbetaservices.ServiceConstants;


@Configuration
public class SwaggerConfig {
	
	@Bean
	public BeanConfig beanConfig (
			final WebApplicationContext context,
			@Value("${isbetaservices.version}") final String version) {
		
		final BeanConfig beanConfig = new BeanConfig();
	    beanConfig.setVersion(version);
	    beanConfig.setBasePath(context.getServletContext().getContextPath());
	    beanConfig.setResourcePackage(ServiceConstants.FQPN + ".rest.v1");
	    beanConfig.setScan(true);
	    
	    return beanConfig;
	}
}
