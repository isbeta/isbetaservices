package com.novartis.nibr.nx.isbetaservices.config;


import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;

import com.google.common.collect.ImmutableList;


@Configuration
@ImportResource("classpath:/security-context.xml")
public class SecurityConfig {
	
	private static final Logger LOG = LoggerFactory.getLogger(SecurityConfig.class);
			

    public final static String HEADER_NIBR521 = "NIBR521";
    public static final String HEADER_NIBREMAIL = "NIBREMAIL";
    public static final String HEADER_NIBRFIRST = "NIBRFIRST";
    public static final String HEADER_NIBRLAST = "NIBRLAST";

    public static final String HEADER_CREDENTIALS = "Cookie";
    
//    @Bean
//	  @Profile("!disable-security")
//    public UserDetailsService userDetailsService () {
//
//    	return new UserDetailsService () {
//
//			@Override
//			public UserDetails loadUserByUsername (final String username) throws UsernameNotFoundException {
//				
//				return new User(username, "", ImmutableList.<GrantedAuthority>of());
//			}	
//    	};
//    }

	@Bean(name={"userDetailsService"})
//	@Profile("disable-security")
    public UserDetailsManager userDetailsManager () {
    	
    	//  an instance of InMemoryUserDetailsManager is almost certainly not what you want to use
    	//  in your actual application code.  more likely you will want to provide a custom implementation 
    	//  of UserDetailsService that returns user info specific to your application.
    	//
    	//  note that this initially empty manager instance WILL NOT WORK if deployed to a Vordel
    	//  mapped VM with security enabled.  this is because no UserDetails 'instance' will be found 
    	//  in the manager.  for projects with no authorization requirements, one option is to provide an
    	//  anonymous UserDetailsService implementation returning User instances with only username set.  
    	//  uncommenting the sections in this file to use this solution.
    	//
    	//  http://docs.spring.io/spring-security/site/docs/current/reference/htmlsingle/#tech-userdetailsservice
    	LOG.warn("returning default instance of InMemoryUserDetailsManager for UserDetailsService");
    	
    	return new InMemoryUserDetailsManager(ImmutableList.<UserDetails>of());
    }
    
    @Bean
    public AuthenticationProvider preauthAuthenticationProvider (final UserDetailsService userDetailsService) {
    	
    	final PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();
    	provider.setPreAuthenticatedUserDetailsService(new UserDetailsByNameServiceWrapper<PreAuthenticatedAuthenticationToken>(userDetailsService));
    	
    	return provider;
    }
    
	@Bean
	@Profile("!disable-security")
	public Filter vordelFilter (final AuthenticationManager authenticationManager) {
		
		final RequestHeaderAuthenticationFilter filter = new RequestHeaderAuthenticationFilter();
		filter.setPrincipalRequestHeader(HEADER_NIBR521);
		filter.setCredentialsRequestHeader(HEADER_CREDENTIALS);
		filter.setAuthenticationManager(authenticationManager);
		filter.setExceptionIfHeaderMissing(false);
		
		return filter;
	}

	@Bean(name={"vordelFilter"})
	@Profile("disable-security")
	public Filter bypassFilter (
			final UserDetailsManager userDetailsManager,
			final AuthenticationManager authenticationManager) {
		
		userDetailsManager.createUser(new User("TESTUSR1", "", ImmutableList.<GrantedAuthority>of()));

		final AbstractPreAuthenticatedProcessingFilter filter = new AbstractPreAuthenticatedProcessingFilter() {
			
			@Override
			protected Object getPreAuthenticatedPrincipal (final HttpServletRequest req) {
				return "TESTUSR1";
			}
			
			@Override
			protected Object getPreAuthenticatedCredentials (final HttpServletRequest req) {
				return "";
			}
		};
		filter.setAuthenticationManager(authenticationManager);
		
		return filter;
	}
	
	@Bean
	public AuthenticationEntryPoint http403EntryPoint () {
		return new Http403ForbiddenEntryPoint();
	}
}
