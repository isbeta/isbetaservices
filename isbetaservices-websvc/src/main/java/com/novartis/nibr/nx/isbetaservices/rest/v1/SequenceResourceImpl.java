package com.novartis.nibr.nx.isbetaservices.rest.v1;

import java.io.IOException;

import javax.ws.rs.core.Response;

import net.novartis.nibr.healthcheck.annotations.AppInfo;
import net.novartis.nibr.healthcheck.annotations.HealthCheck;
import net.novartis.nibr.healthcheck.annotations.Scope;
import net.novartis.nibr.healthcheck.annotations.UseCase;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.novartis.nibr.nx.services.rest.AbstractResource;

@Component
@HealthCheck(
		//  appInfo annotation should only be used for one HealthcCheck per application
        appInfo = @AppInfo(appId = "isbetaservices", appName = "isbetaservices-websvc", appVersion = "0.1.0-SNAPSHOT", environmentSysProp="nibr.deploy.env"),
        description = "checks reachability",
        scope = {Scope.FULL, Scope.LB, Scope.MONITOR, Scope.QUICK}
)
@UseCase("checkSequenceResource")
public class SequenceResourceImpl extends AbstractResource 
implements SequenceResource {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SequenceResourceImpl.class);

	@Override
	public Response sequenceOPTIONS() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response countSequenceLength(String sequence) {
		try {
			JSONObject json = new JSONObject();
			json.put("SequenceCount", sequence.length());
			return Response.status(Response.Status.OK).entity(json.toString()).build();
		} catch (Exception e) {
			LOGGER.debug("countSequenceLength " + e.getMessage());
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	public Response countSequenceLengthPost(String sequence) {
		try {
			JSONObject json = new JSONObject();
			json.put("SequenceCount", sequence.length());
			return Response.status(Response.Status.OK).entity(json.toString()).build();
		} catch (Exception e) {
			LOGGER.debug("countSequenceLengthPost " + e.getMessage());
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

}
