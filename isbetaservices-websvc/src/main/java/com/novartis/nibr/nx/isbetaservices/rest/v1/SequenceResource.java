package com.novartis.nibr.nx.isbetaservices.rest.v1;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path(SequenceResource.PATH)
@Api(tags={SequenceResource.PATH})
public interface SequenceResource {
	
	String PATH = "/v1/commands/Sequence";
	
	@OPTIONS
	@Path("/")
	Response sequenceOPTIONS ();
	

	@GET
	@Path("/count/{c}")
	@Produces({APPLICATION_JSON, APPLICATION_XML})
	@ApiOperation(value = "Reads sequence and generates the length of the sequence", httpMethod = "GET", response = Response.class, responseContainer = "JSON")
	Response countSequenceLength(@ApiParam(value = "Sequence", required = true) @PathParam("c") String sequence);

	@POST
	@Consumes({APPLICATION_JSON, APPLICATION_XML,APPLICATION_FORM_URLENCODED})
	@Produces({APPLICATION_JSON, APPLICATION_XML})
	@Path("/count")
	@ApiOperation(value = "Reads sequence and generates the length of the sequence", httpMethod = "POST", response = Response.class, responseContainer = "JSON")
	Response countSequenceLengthPost(@ApiParam(value = "Sequence", required = true) @FormParam(value = "Sequence") String sequence);
}
