package com.novartis.nibr.nx.isbetaservices.rest.v1;


import javax.validation.constraints.Min;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import static javax.ws.rs.core.MediaType.*;
import com.novartis.nibr.nx.isbetaservices.entity.MyEntity;


@Path(MyEntityResource.PATH)
@Api(tags={MyEntityResource.PATH})
public interface MyEntityResource {
	
	String PATH = "/v1/myEntities";
	
	String PARAM_ENTITY_ID = "entityId";
	String PARAM_ENTITY_NAME = "entityName";

	@OPTIONS
	@Path("/")
	Response myEntitiesOPTIONS ();

	@GET
	@Produces({APPLICATION_JSON, APPLICATION_XML})
	@Path("/")
	@ApiOperation(value="list entities")
	Response myEntitiesGET (
			@Min(1) @QueryParam("page") @DefaultValue("1") @ApiParam("1-based page index") Integer page,
			@Min(1) @QueryParam("itemsPerPage") @DefaultValue("10") Integer itemsPerPage,
			@QueryParam("sort") @ApiParam("sort directive in the format {property name}:asc|desc") String sortString);

	@POST
	@Consumes({APPLICATION_JSON, APPLICATION_XML})
	@Produces({APPLICATION_JSON, APPLICATION_XML})
	@Path("/")
	@ApiOperation(value="create an entity")
	Response myEntitiesPOST (
			MyEntity entity);

	@OPTIONS
	@Path("/{" + PARAM_ENTITY_ID + "}")
	Response myEntityOPTIONS ();
	
	@GET
	@Produces({APPLICATION_JSON, APPLICATION_XML})
	@Path("/{" + PARAM_ENTITY_ID + "}")
	@ApiOperation(value="find one entity")
	Response findOneGET (
			@PathParam(PARAM_ENTITY_ID) Long entityId);

	@PUT
	@Consumes({APPLICATION_JSON, APPLICATION_XML})
	@Produces({APPLICATION_JSON, APPLICATION_XML})
	@Path("/{" + PARAM_ENTITY_ID + "}")
	@ApiOperation(value="save one entity")
	Response saveOnePost (
			@PathParam(PARAM_ENTITY_ID) Long entityId,
			MyEntity entity);

	@DELETE
	@Path("/{" + PARAM_ENTITY_ID + "}")
	@ApiOperation(value="delete one entity")
	Response deleteOneDELETE (
			@PathParam(PARAM_ENTITY_ID) Long entityId);

	
	@GET
	@Produces({APPLICATION_JSON, APPLICATION_XML})
	@Path("/name/{" + PARAM_ENTITY_NAME + "}")
	Response findByEntityNameGET (
			@PathParam(PARAM_ENTITY_NAME) String entityName);
}
