package com.novartis.nibr.nx.isbetaservices.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;


@Configuration
public class WebsvcConfig {
    
	/**
	 * http://www.springbyexample.org/examples/spring-data-jpa-auditing-code-example.html
	 * 
	 * @see AuditorAware
	 * @see EnableJpaAuditing
	 * 
	 * @author ringgjo1
	 */
	@Bean
	public AuditorAware<String> auditorAware () {
		
		return new AuditorAware<String>() {

			@Override
			public String getCurrentAuditor () {
				return SecurityContextHolder.getContext().getAuthentication().getName();
			}
		};
	}
	
}
