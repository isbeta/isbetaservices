package com.novartis.nibr.nx.isbetaservices.rest.v1;


import static javax.ws.rs.HttpMethod.*;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.google.common.base.Joiner;

import net.novartis.nibr.healthcheck.annotations.AppInfo;
import net.novartis.nibr.healthcheck.annotations.Check;
import net.novartis.nibr.healthcheck.annotations.HealthCheck;
import net.novartis.nibr.healthcheck.annotations.Scope;
import net.novartis.nibr.healthcheck.annotations.UseCase;

import com.novartis.nibr.nx.services.entity.DomainUtils;
import com.novartis.nibr.nx.services.rest.AbstractResource;

import com.novartis.nibr.nx.isbetaservices.entity.MyEntity;
import com.novartis.nibr.nx.isbetaservices.service.MyEntityService;


@Component
@HealthCheck(
		//  appInfo annotation should only be used for one HealthcCheck per application
        appInfo = @AppInfo(appId = "isbetaservices", appName = "isbetaservices-websvc", appVersion = "0.1.0-SNAPSHOT", environmentSysProp="nibr.deploy.env"),
        description = "checks reachability",
        scope = {Scope.FULL, Scope.LB, Scope.MONITOR, Scope.QUICK}
)
@UseCase("checkMyEntityResource")
public class MyEntityResourceImpl 
	extends AbstractResource 
	implements MyEntityResource {
		
	private final MyEntityService myEntityService;
	
	@Autowired
	public MyEntityResourceImpl (final MyEntityService myEntityService) {
		super();
		
		this.myEntityService = myEntityService;
	}
	
	@Override
	public Response myEntitiesOPTIONS () {
		return options(GET, POST, OPTIONS);
	}

	@Override
	public Response myEntitiesGET (
			final Integer page,
			final Integer itemsPerPage,
			final String sortString) {

		//  note that the NIBR web api guide specifies 1-based page indexes, while 
		//  PageRequest uses 0-based
		final PageRequest pr = new PageRequest(page - 1, itemsPerPage, (sortString != null) ? DomainUtils.parseSort(sortString) : null);
		return Response.ok(myEntityService.findAll(pr)).build();
	}
	
	@Override
	public Response myEntitiesPOST (
			final MyEntity entity) {
		return Response.ok(myEntityService.save(entity)).build();
	}

	@Override
	public Response myEntityOPTIONS () {
		return options(GET, POST, DELETE, OPTIONS);
	}
	
	@Override
	public Response findOneGET (
			final Long entityID) {

		final MyEntity me = myEntityService.findOne(entityID);
		return (me != null) ? Response.ok(me).build() : Response.status(Status.NOT_FOUND).build();
	}
	
	@Override
	public Response saveOnePost (
			final Long entityId,
			final MyEntity entity) {
		entity.setEntityId(entityId);
		return Response.ok(myEntityService.save(entity)).build();
	}

	@Override
	public Response deleteOneDELETE (
			final Long entityId) {

		final MyEntity me = myEntityService.findOne(entityId);
		if (me != null) {
			myEntityService.delete(entityId);
			return Response.noContent().build();  
		}

		return Response.status(Status.NOT_FOUND).build();
	}

	@Override
	public Response findByEntityNameGET (
			final String entityName) {	
		final MyEntity me = myEntityService.findByEntityName(entityName);
		return (me != null) ? Response.ok(me).build() : Response.status(Status.NOT_FOUND).build();
	}

	@Check
	public void findOneCheck () {
		myEntityService.findOne(1001L);
	}
}
